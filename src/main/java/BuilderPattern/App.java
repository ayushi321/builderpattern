package BuilderPattern;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Student st1 = new Student.StudentBuilder("Denny","Chan").age(23).address("Newyork").build();
        System.out.println(st1);

        Student st2 = new Student.StudentBuilder("Yamini","Gautam").address("Mumbai").build();
        System.out.println(st2);

        Student st3 = new Student.StudentBuilder("Kristan","Stevart").build();
        System.out.println(st3);
    }
}
