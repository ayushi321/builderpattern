package BuilderPattern;


public class Student  {
    private String firstname;
    private String lastname;
    private int age;
    private String address;

    private Student(StudentBuilder builder){
        this.firstname = builder.firstname;
        this.lastname = builder.lastname;
        this.age = builder.age;
        this.address = builder.address;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }
    public String toString(){
        return "Student: "+this.firstname+" "+this.lastname+","+this.age+","+this.address;
    }

    public  static class StudentBuilder {
        private String firstname;
        private String lastname;
        private int age;
        private String address;


        public StudentBuilder(String firstname, String lastname) {
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public StudentBuilder age(int age) {
            this.age = age;
            return this;
        }

        public StudentBuilder address(String address) {
            this.address = address;
            return this;
        }
        public Student build(){
            Student st = new Student(this);
            return st;
        }

    }
}
